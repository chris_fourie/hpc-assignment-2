#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>

#include <helper_cuda.h>
#include <helper_string.h> //sdkFindFilePath()
#include <helper_image.h> //sdkLoadPGM() sdkSavePGM()*
#include <helper_functions.h>

#include "tex_img_conv.h"

#include <string.h>
#include <math.h>

#include <assert.h> //macro to add diagnostics i.e. void assert(int expression) e.g  	assert(num is odd)  -> assert(num %2 == 1)

#include <time.h>
//http://cuda-programming.blogspot.com/2013/02/texture-memory-in-cuda-what-is-texture.html

texture<float , 2 , cudaReadModeElementType> tex_in;
//*##*//
//*##*//
#include <cuda_profiler_api.h>

int main(){

// Size / File
const char* file_Names[] = {"bikes.pgm","lena_bw.pgm","flower.pgm","forrest.pgm","aurora.pgm"} ;

//Masktype


//RESULTS to file
FILE * fp;

     fp = fopen ("results.csv","w+");
    fprintf (fp , "f , size , mask type , mask size , kernel time , Mpix/s , CPU time , overhead, \n");
    const char* fileEnding[5];
    fileEnding[0] = "_out_1_edge(3x3)-texture.pgm";
    fileEnding[1] = "_out_2_ave(3x3)-texture.pgm";
    fileEnding[2] = "_out_3_ave(5x5)-texture.pgm";
    fileEnding[3] = "_out_4_sharp(3x3)-texture.pgm";
    fileEnding[4] = "_out_5_sharp(5x5)-texture.pgm";

for (int file = 0; file < 5; file++){
for (int mask_Type = 0; mask_Type < 5; mask_Type++){
fprintf (fp , "f , ");


cudaProfilerStart();

    checkCudaErrors(cudaDeviceSynchronize());
    StopWatchInterface *timer_O = NULL;
    sdkCreateTimer(&timer_O);
    sdkStartTimer(&timer_O);


  //DATA//
  const char* fileName = file_Names[file];
  float* h_Data = NULL ;
  uint* width ;  uint* height;
  uint width_val = 0; uint height_val = 0;
  width = &width_val; height = &height_val;


  //MASK//
  uint mask_Size;
  if (mask_Type == 2 || mask_Type == 4) {
      mask_Size = 5;
  }
  else{
      mask_Size = 3;
  }


  // IMPORT IMAGE //
  char* imagePath = sdkFindFilePath(fileName,""); //second parameter is optional but must be present
  sdkLoadPGM(imagePath , &h_Data , width , height);//read from imagePath - write to h_Data , width , height
  // printf("%s\n","image imported ");

  fprintf (fp,"%u x %u , ",(*width),(*height));


/////////////////
//Image Padding//
/////////////////
float* h_Data_Pad;
assert(mask_Size %2 == 1) ;//ensure that mask_Size is an odd number
uint padding_Size = (mask_Size-1)/2 ;//padding at edge -> total padding in a dimension = 2*padding_Size

uint padded_Matrix_TotalWidth = (*width) + padding_Size*2;
uint padded_Matrix_TotalHeight = (*height) + padding_Size*2; //assumption is that masks are square , therefore padding_Size = paddding_Height
h_Data_Pad = (float *) calloc(padded_Matrix_TotalWidth*padded_Matrix_TotalHeight , sizeof(float)); //calloc - like malloc but set all elements to 0  https://www.geeksforgeeks.org/calloc-versus-malloc/

//read h_Data into h_Data_Pad
for (uint row = 0; row<(*height); row++){
    for (uint col = 0; col<(*width); col++){
        h_Data_Pad[(row+padding_Size)*(padded_Matrix_TotalWidth)+(col+padding_Size)] = h_Data[row*(*width) + col];
    }
}
//end Image Padding
///////////////////

//MASKS//
float* mask = (float *) malloc((mask_Size)*(mask_Size)*sizeof(float));



if (mask_Type == 0)
{
    fprintf (fp,"edge , 3x3 , ");
//edge detection
mask[0] = -1.0;
mask[1] = 0.0;
mask[2] = 1.0;
mask[3] = -2.0;
mask[4] = 0.0;
mask[5] = 2.0;
mask[6] = -1.0;
mask[7] = 0.0;
mask[8] = 1.0;
}

if (mask_Type == 1)
{
    fprintf (fp,"ave , 3x3 , ");
// //averaging
mask[0] = 1.0/(mask_Size*mask_Size);
mask[1] = 1.0/(mask_Size*mask_Size);
mask[2] = 1.0/(mask_Size*mask_Size);
mask[3] = 1.0/(mask_Size*mask_Size);
mask[4] = 1.0/(mask_Size*mask_Size);
mask[5] = 1.0/(mask_Size*mask_Size);
mask[6] = 1.0/(mask_Size*mask_Size);
mask[7] = 1.0/(mask_Size*mask_Size);
mask[8] = 1.0/(mask_Size*mask_Size);
//3x3
}


if (mask_Type == 2)
{
    fprintf (fp,"ave , 5x5 , ");
mask[0] = 1.0/(mask_Size*mask_Size);
mask[1] = 1.0/(mask_Size*mask_Size);
mask[2] = 1.0/(mask_Size*mask_Size);
mask[3] = 1.0/(mask_Size*mask_Size);
mask[4] = 1.0/(mask_Size*mask_Size);
mask[5] = 1.0/(mask_Size*mask_Size);
mask[6] = 1.0/(mask_Size*mask_Size);
mask[7] = 1.0/(mask_Size*mask_Size);
mask[8] = 1.0/(mask_Size*mask_Size);
mask[9] = 1.0/(mask_Size*mask_Size);
mask[10] = 1.0/(mask_Size*mask_Size);
mask[11] = 1.0/(mask_Size*mask_Size);
mask[12] = 1.0/(mask_Size*mask_Size);
mask[13] = 1.0/(mask_Size*mask_Size);
mask[14] = 1.0/(mask_Size*mask_Size);
mask[15] = 1.0/(mask_Size*mask_Size);
mask[16] = 1.0/(mask_Size*mask_Size);
mask[17] = 1.0/(mask_Size*mask_Size);
mask[18] = 1.0/(mask_Size*mask_Size);
mask[19] = 1.0/(mask_Size*mask_Size);
mask[20] = 1.0/(mask_Size*mask_Size);
mask[21] = 1.0/(mask_Size*mask_Size);
mask[22] = 1.0/(mask_Size*mask_Size);
mask[23] = 1.0/(mask_Size*mask_Size);
mask[24] = 1.0/(mask_Size*mask_Size);
//5x5
}

if (mask_Type == 3)
{
    fprintf (fp,"sharp , 3x3 , ");
// Sharpening -> must sum to 1 along the axes
mask[0] = -1.0;
mask[1] = -1.0;
mask[2] = -1.0;
mask[3] = -1.0;
mask[4] = 9.0;
mask[5] = -1.0;
mask[6] = -1.0;
mask[7] = -1.0;
mask[8] = -1.0;
//3x3
}

if (mask_Type == 4)
{
    fprintf (fp,"sharp , 5x5 , ");
mask[0] = -1.0;
mask[1] = -1.0;
mask[2] = -1.0;
mask[3] = -1.0;
mask[4] = -1.0;
mask[5] = -1.0;
mask[6] = -1.0;
mask[7] = -1.0;
mask[8] = -1.0;
mask[9] = -1.0;
mask[10] = -1.0;
mask[11] = -1.0;
mask[12] = 26.0;
mask[13] = -1.0;
mask[14] = -1.0;
mask[15] = -1.0;
mask[16] = -1.0;
mask[17] = -1.0;
mask[18] = -1.0;
mask[19] = -1.0;
mask[20] = -1.0;
mask[21] = -1.0;
mask[22] = -1.0;
mask[23] = -1.0;
mask[24] = -1.0;
//5x5
}

//*##*//
//*##*//

//////////////////////////
// Pass Data to Device //
/////////////////////////
//TEX
// Allocate array and copy image data
cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32 , 0 , 0 , 0 , cudaChannelFormatKindFloat);
cudaArray *cuArray;
checkCudaErrors(cudaMallocArray(&cuArray,
                                &channelDesc,
                                (padded_Matrix_TotalWidth),
                                (padded_Matrix_TotalHeight)));
checkCudaErrors(cudaMemcpyToArray(cuArray,
                                  0,
                                  0,
                                  h_Data_Pad,
                                  (padded_Matrix_TotalWidth*padded_Matrix_TotalHeight)*sizeof(float),
                                  cudaMemcpyHostToDevice));
//TEX
// Set texture parameters
tex_in.addressMode[0] = cudaAddressModeWrap;
tex_in.addressMode[1] = cudaAddressModeWrap;
tex_in.filterMode = cudaFilterModeLinear;
tex_in.normalized = false;    // access with normalized texture coordinates

//TEX
// Bind the array to the texture
checkCudaErrors(cudaBindTextureToArray(tex_in , cuArray , channelDesc));

float *d_Data_Out = NULL;
float *d_Mask = NULL;
checkCudaErrors(cudaMalloc((void **) &d_Data_Out , (*width)*(*height)*sizeof(float)));
checkCudaErrors(cudaMalloc((void **) &d_Mask , mask_Size*mask_Size*sizeof(float)));

dim3 dimBlock(8,8);
dim3 dimGrid(*width / dimBlock.x , *height / dimBlock.y);

HANDLE_ERROR(cudaMemcpy(d_Mask , mask,(mask_Size*mask_Size)*sizeof(float),cudaMemcpyHostToDevice));


//TIMER START//
checkCudaErrors(cudaDeviceSynchronize());
StopWatchInterface *timer = NULL;
sdkCreateTimer(&timer);
sdkStartTimer(&timer);

//KERNEL//
d_tex_convolve<<<dimGrid,dimBlock>>>(d_Data_Out,*width , *height , d_Mask , mask_Size);

getLastCudaError("Kernel execution failed");
checkCudaErrors(cudaDeviceSynchronize());
sdkStopTimer(&timer);

//TIMER END//

//////////////////////////
// Get Data from Device //
/////////////////////////
float* h_Data_Con = (float *) malloc((*width)*(*height)*sizeof(float));
HANDLE_ERROR(cudaMemcpy(h_Data_Con , d_Data_Out , (*width)*(*height)*sizeof(float) , cudaMemcpyDeviceToHost)) ;

////////////////////////////////
//Save image in h_data to file//
////////////////////////////////

char outputFilename[1024];
    strcpy(outputFilename , imagePath);
    strcpy(outputFilename + strlen(imagePath) - 4 ,  fileEnding[mask_Type]);
    sdkSavePGM(outputFilename , h_Data_Con , *width , *height);
printf("image exported as '%s'\n" , outputFilename);


//clean up
free (h_Data);
free (h_Data_Pad);
free (h_Data_Con);
//HANDLE_ERROR(cudaFree(d_Data_In));
cudaUnbindTexture(tex_in);
checkCudaErrors(cudaFreeArray(cuArray));
HANDLE_ERROR(cudaFree(d_Data_Out));
HANDLE_ERROR(cudaFree(d_Mask));

//*##*//
//*##*//
// printf("GPU time: %.3f (ms)\n" , sdkGetTimerValue(&timer));
// printf("%.2f Mpixels/sec\n\n",((*width) *(*height) / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6);
fprintf(fp,"%.3f , " , sdkGetTimerValue(&timer));
fprintf(fp,"%.2f , ",((*width) *(*height) / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6);

checkCudaErrors(cudaDeviceSynchronize());
sdkStopTimer(&timer_O);
// printf("CPU time: %.2f (ms)\n" , sdkGetTimerValue(&timer_O));
// printf("Overhead: %.2f (ms) \n" , sdkGetTimerValue(&timer_O) - sdkGetTimerValue(&timer));

fprintf(fp," %.3f , " , sdkGetTimerValue(&timer_O));
fprintf(fp," %.3f , " , sdkGetTimerValue(&timer_O) - sdkGetTimerValue(&timer));

sdkDeleteTimer(&timer);
sdkDeleteTimer(&timer_O);

cudaProfilerStop();

fprintf(fp,"\n");
}//end FOR 2 mask type
}//end FOR 1 File
fclose (fp);
//*##*//
//*##*//

return 0;
} // //END OF MAIN()


///////////////
// FUNCTIONS //
///////////////


__global__ void d_tex_convolve (float* out_Data , uint data_Width , uint data_Height , const float* mask , uint mask_Size)
{
    //uint padding_Size = (mask_Size-1)/2;
    float temp = 0.0;

    int i_Out_Row = blockIdx.x * blockDim.x + threadIdx.x;
    int i_Out_Col = blockIdx.y * blockDim.y + threadIdx.y;

    float offset = 0.5f ;//(float)padding_Size;

    //temp += tex2D(tex_in,((float)i_Out_Row),((float)i_Out_Col));

    for (size_t mask_row = 0; mask_row < mask_Size; mask_row++) {
        for (size_t mask_col = 0; mask_col < mask_Size; mask_col++) {
          temp += tex2D(tex_in,((float)i_Out_Col+offset+mask_col),((float)i_Out_Row+offset+mask_row)) * mask[(mask_row*mask_Size)+mask_col];
            //temp += tex2D(tex_in,((float)i_Out_Row+offset+mask_row)*(data_Width+2*padding_Size) + ((float)i_Out_Col+offset+mask_col)) * mask[(mask_row*mask_Size)+mask_col];
        }
    }
    if (temp > 1.0) temp = 1.0;
    else if(temp < 0.0) temp = 0.0;
    out_Data[i_Out_Row*data_Width + i_Out_Col] = temp;
}
