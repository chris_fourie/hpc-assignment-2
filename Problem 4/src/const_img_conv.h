//serial_img_conv.h

#ifndef SERIAL_IMG_CONV_H // from example - why are these important ??
#define SERIAL_IMG_CONV_H

__host__ void importImage_Data(const char* fileName,float* h_Data, uint* width, uint* height);

__host__ void export_Image_Data (const char* fileName, float* h_OutputData, uint width, uint height);

__host__ void convolve (float* in_Data, float* out_Data, uint data_Width, uint data_Height, const float* mask, uint mask_Size);

__global__ void d_naive_convolve (float* in_Data, float* out_Data, uint data_Width, uint data_Height, const float* mask, uint mask_Size);

__global__ void d_shared_convolve (float* in_Data, float* out_Data, uint data_Width, uint data_Height, const float* mask, uint mask_Size);

__global__ void d_const_convolve (float* in_Data , float* out_Data , uint data_Width , uint data_Height , uint mask_Size); 


static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
    if (err != cudaSuccess) {
        printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_FAILURE );
    }
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

#endif
